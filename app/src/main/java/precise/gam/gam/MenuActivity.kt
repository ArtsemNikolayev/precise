package precise.gam.gam

import android.content.*
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.amplitude.api.Amplitude
import com.appsflyer.AppsFlyerLib
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_menu.*
import precise.gam.gam.MainActivity
import precise.gam.gam.R
import precise.gam.gam.game.Activity
import precise.gam.gam.game.newLink
import java.util.*
import kotlin.collections.HashMap


var PACKAGE_NAME: String = ""
var DEVICE_TOKEN = ""
var LOCALE = ""

var isReceive = true

var showFantik = true

class MenuActivity : AppCompatActivity(), MenuContract {
    private lateinit var presenter: MenuPresenter

    private lateinit var startGameButton: FrameLayout
    private lateinit var webViewActivityButton: ImageView
    private lateinit var progressBar: ProgressBar
    private lateinit var game: FrameLayout

    lateinit var receiver1: BroadcastReceiver
    lateinit var receiver2: BroadcastReceiver

    private var savedLink = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_menu)

            presenter = MenuPresenter(this)

            init()

            setOnClickListener()

            PACKAGE_NAME = application.packageName
            DEVICE_TOKEN = FirebaseInstanceId.getInstance().getToken().toString()
            LOCALE = Locale.getDefault().getDisplayLanguage()

            progressBar = findViewById(R.id.progress_bar)
            game = findViewById(R.id.game)

            Handler().postDelayed({
                if (newLink != "") {
                    if (isReceive) {
                        startActivity(Intent(this, Activity::class.java))
                        isReceive = false
                    }
                }

            }, 28000L)

            Handler().postDelayed({
                if (savedLink == "") {
                    progressBar.visibility = View.INVISIBLE
                    bg11.visibility = View.INVISIBLE
                    game.visibility = View.VISIBLE
                    sendFantikToAmplitude()

                }

            }, 30000L)


    }

    private fun checkSave() {
            var pref = getPreferences(Context.MODE_PRIVATE)
            savedLink = pref.getString("link", "").toString()

            if (savedLink != "") {
                isReceive = false
                newLink = savedLink!!
                if (checkConnection()) {
                    startActivity(Intent(this, Activity::class.java))
                } else {
                    AlertDialog.Builder(this)
                        .setMessage("No internet connection")
                        .setPositiveButton("OK", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, p1: Int) {
                                finish()
                            }

                        })
                        .setCancelable(false)
                        .show()
                }

            }

    }

    fun checkConnection(): Boolean {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo != null
    }

    private fun init() {
        startGameButton = findViewById(R.id.play_button)
        webViewActivityButton = findViewById(R.id.button)
    }

    private fun setOnClickListener() {
        startGameButton.setOnClickListener { presenter.onStartGameClick() }
    }

    override fun startGameActivity(timeInMillis: Long) {
        startGameButton.animate()
            .scaleXBy(-0.1F)
            .scaleYBy(-0.1F)
            .setDuration(200L)
            .withEndAction {
                startGameButton.animate()
                    .scaleXBy( 0.1F)
                    .scaleYBy( 0.1F)
                    .setDuration(200L)
                    .start()
            }
            .start()

        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
        }, timeInMillis)
    }

    override fun startActivity(timeInMillis: Long) {
        Handler().postDelayed({
            startActivity(Intent(this, Activity::class.java))
        }, timeInMillis)
    }

    override fun animateWebViewButton(timeInMillis: Long) {
        webViewActivityButton.animate()
            .rotationYBy(360F)
            .setDuration(timeInMillis)
            .start()
    }

    override fun setAllDisabled() {
        webViewActivityButton.isEnabled = false
        startGameButton.isEnabled = false
    }

    override fun onResume() {
        super.onResume()
            overridePendingTransition(android.R.anim.fade_in, 1)
            webViewActivityButton.isEnabled = true
            startGameButton.isEnabled = true

            registerReceiver()
            timerReceiver()

            checkSave()
    }

    private fun registerReceiver() {
        receiver1 = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (newLink != "") {
                    if (isReceive) {
                        startActivity(Intent(this@MenuActivity, Activity::class.java))
                        isReceive = false
                    }
                    webViewActivityButton.setOnClickListener {
                        presenter.onButtonViewClick()
                    }
                } else {
                    progressBar.visibility = View.INVISIBLE
                    bg11.visibility = View.INVISIBLE
                    game.visibility = View.VISIBLE
                    sendFantikToAmplitude()

                }
            }
        }
        var action = IntentFilter("receiver")
        registerReceiver(receiver1, action)

    }

    private fun timerReceiver() {
        receiver2 = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent) {
                var time: Long = intent.getLongExtra("time", 10000)
                Handler().postDelayed({
                    if (newLink != "") {
                        if (isReceive) {
                            startActivity(Intent(this@MenuActivity, Activity::class.java))
                            isReceive = false
                        }
                        webViewActivityButton.setOnClickListener {
                            presenter.onButtonViewClick()
                        }
                    } else {
                        progressBar.visibility = View.INVISIBLE
                        bg11.visibility = View.INVISIBLE
                        game.visibility = View.VISIBLE
                        sendFantikToAmplitude()
                    }
                }, time)
            }
        }
        var action = IntentFilter("timer")
        registerReceiver(receiver2, action)


        if (newLink != "") {
            webViewActivityButton.setOnClickListener {
                presenter.onButtonViewClick()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (newLink != "") {
            saveLink()
        }
        showFantik = false
    }

    private fun saveLink() {
        val save = getPreferences(Context.MODE_PRIVATE)
        val editor = save.edit()
        editor.putString("link", newLink)
        editor.commit()
        var link = getPreferences(Context.MODE_PRIVATE).getString("link", "")

        Log.e("save", link)
    }

    private fun sendFantikToAmplitude() {
        if (showFantik) {
            Amplitude.getInstance().logEvent("Start Fantik")

            var map: HashMap<String, Any> = HashMap()
            map.put("fangame", "fangame")
            AppsFlyerLib.getInstance().trackEvent(this, "Start Fantik", map)
        }
    }

}
