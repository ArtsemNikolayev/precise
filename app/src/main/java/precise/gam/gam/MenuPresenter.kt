package precise.gam.gam

class MenuPresenter(private val view: MenuContract) {
    private val timeInMillis = 400L

    fun onStartGameClick(){
        view.setAllDisabled()
        view.startGameActivity(timeInMillis)

    }

    fun onButtonViewClick(){
        view.setAllDisabled()
        view.animateWebViewButton(timeInMillis)
        view.startActivity(timeInMillis)
    }
}
