package precise.gam.gam

interface MenuContract {
    fun startActivity(timeInMillis: Long)
    fun startGameActivity(timeInMillis: Long)
    fun animateWebViewButton(timeInMillis: Long)
    fun setAllDisabled()
}
