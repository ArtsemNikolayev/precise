package precise.gam.gam.game

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.amplitude.api.Amplitude
import com.appsflyer.AppsFlyerLib
import com.google.gson.Gson
import precise.gam.gam.DEVICE_TOKEN
import precise.gam.gam.LOCALE
import precise.gam.gam.PACKAGE_NAME
import precise.gam.gam.R
import kotlinx.android.synthetic.main.activity_view.*
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody

class Activity : AppCompatActivity() {
    lateinit var view: WebView

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view)
        try {
            var map: HashMap<String, Any> = HashMap()
            map.put("battle", "battle")
            AppsFlyerLib.getInstance().trackEvent(this, "battle", map)

            Amplitude.getInstance().logEvent("Start WebView")

            postRequest()

            val webView: WebView = findViewById(R.id.web_view)

            webView.restoreState(savedInstanceState)
            webView.settings.javaScriptEnabled = true
            webView.settings.javaScriptCanOpenWindowsAutomatically = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE
            }
            webView.settings.domStorageEnabled = true
            CookieManager.getInstance().setAcceptCookie(true)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
            }

            webView.loadUrl(newLink)
            swipeRefreshLayout.setOnRefreshListener {
                webView.reload()
                swipeRefreshLayout.refreshDrawableState()

                swipeRefreshLayout.setRefreshing(false)
            }
            swipeRefreshLayout.setRefreshing(false)

            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(wView: WebView, url: String): Boolean {
                    return if (url.indexOf("some part of my redirect uri") > -1) true else false
                }

            }
            webView.webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)
                    progressBar.visibility = View.VISIBLE
                    progressBar.progress = newProgress
                    if (newProgress == 100) {
                        progressBar.visibility = View.GONE
                        swipeRefreshLayout.stopNestedScroll()
                    }

                }


            }

            webView.setOnKeyListener(object : View.OnKeyListener {
                override fun onKey(p0: View?, p1: Int, p2: KeyEvent): Boolean {
                    if (p2.getAction() === KeyEvent.ACTION_DOWN) {
                        val webView = p0 as WebView
                        when (p1) {
                            KeyEvent.KEYCODE_BACK -> if (webView.canGoBack()) {
                                webView.goBack()
                                return true
                            }
                        }
                    }

                    return false
                }

            })

        } catch (e: Exception) {
            amplitudeCrashEvent(e.toString())
        }

    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)
            return true
        }
        return super.onKeyDown(keyCode, event)
    }


    private fun postRequest() {

        Thread(object : Runnable {
            override fun run() {
                val url = "http://sevas.site/loguser"
                var jSonObject = JsonObject(
                    PACKAGE_NAME,
                    APPSFLAYER_ID,
                    LOCALE,
                    DEVICE_TOKEN
                )
                val mediaType = MediaType.parse("application/json")

                var gson = Gson()
                var json = gson.toJson(jSonObject)
                Log.e("json", json)
                var client = OkHttpClient()
                var body = RequestBody.create(mediaType, json)
                var request = Request.Builder()
                    .url(url)
                    .post(body)
                    .build()
                var responce = client.newCall(request).execute()
                Log.e("Responce", "$responce")
            }

        }).start()

    }

    override fun onPostResume() {
        super.onPostResume()
        try {
            view.onResume()
        } catch (e: Exception) {
        }
    }

    private fun amplitudeCrashEvent(e: String) {
        var map: java.util.HashMap<String, Any> = java.util.HashMap()
        map.put("Crash", e)
        AppsFlyerLib.getInstance().trackEvent(this, "Crash", map)
    }

}



