package precise.gam.gam.game

data class JsonObject(
    var appBundle: String,
    var deviceId: String,
    var locale: String,
    var deviceToken: String
)
