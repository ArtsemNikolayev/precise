package precise.gam.gam

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.SeekBar
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var list1 = ArrayList<Int>()
    var list2 = ArrayList<Int>()

    var rand = 25

    var lvl = 0
    var width = 0

    var image1 = 0
    var image2 = 0

    var _width = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_main)


        list2.add(R.drawable.q1)
        list2.add(R.drawable.q2)
        list2.add(R.drawable.q3)
        list2.add(R.drawable.q4)
        list2.add(R.drawable.q5)
        list2.add(R.drawable.q6)
        list2.add(R.drawable.q7)
        list2.add(R.drawable.q8)
        list2.add(R.drawable.q9)
        list2.add(R.drawable.q10)
        list2.add(R.drawable.q11)
        list2.add(R.drawable.q12)
        list2.add(R.drawable.q13)
        list2.add(R.drawable.q14)
        list2.add(R.drawable.q15)
        list2.add(R.drawable.q16)
        list2.add(R.drawable.q17)
        list2.add(R.drawable.q18)
        list2.add(R.drawable.q19)
        list2.add(R.drawable.q20)
        list2.add(R.drawable.q21)
        list2.add(R.drawable.q22)
        list2.add(R.drawable.q23)
        list2.add(R.drawable.q24)
        list2.add(R.drawable.q25)
        list2.add(R.drawable.q26)

        list1.add(R.drawable.w1)
        list1.add(R.drawable.w2)
        list1.add(R.drawable.w3)
        list1.add(R.drawable.w4)
        list1.add(R.drawable.w5)
        list1.add(R.drawable.w6)
        list1.add(R.drawable.w7)
        list1.add(R.drawable.w8)
        list1.add(R.drawable.w9)
        list1.add(R.drawable.w10)
        list1.add(R.drawable.w11)
        list1.add(R.drawable.w12)
        list1.add(R.drawable.w13)
        list1.add(R.drawable.w14)
        list1.add(R.drawable.w15)
        list1.add(R.drawable.w16)
        list1.add(R.drawable.w17)
        list1.add(R.drawable.w18)
        list1.add(R.drawable.w19)
        list1.add(R.drawable.w20)
        list1.add(R.drawable.w21)
        list1.add(R.drawable.w22)
        list1.add(R.drawable.w23)
        list1.add(R.drawable.w24)
        list1.add(R.drawable.w25)
        list1.add(R.drawable.w26)


        check.setOnClickListener(this)

        retry.setOnClickListener {
            retry.animate()
                .rotationY(360F)
                .setDuration(500)
                .start()
            you_lose.animate()
                .translationY(-2000F)
                .setDuration(500)
                .start()
            Handler().postDelayed({
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }, 800)

        }
        next.setOnClickListener {
            next.animate()
                .translationX(-1000F)
                .setDuration(500)
                .start()
            you_win.animate()
                .translationX(1000F)
                .setDuration(500)
                .start()

            Handler().postDelayed({
                you_win.x = you_lose.x
                next.x = retry.x
                randomImage()
            }, 600)
        }

        size.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                var w = width

                if (p1 > 50) {
                    w = width + ((p1 - 50) * 8)
                    Log.e("$width", "$w")
                } else if (p1 < 50) {
                    w = width - ((50 - p1) * 8)
                }
                Log.e("FFFF", "${p1}")
                setImageWidth(w)


            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })
        score.visibility = View.INVISIBLE
        check.visibility = View.INVISIBLE
        size.visibility = View.INVISIBLE
        imageView2.visibility = View.INVISIBLE
        Handler().postDelayed({
            randomImage()

        }, 1000)


    }

//    private fun setNewWidth(w: Int){
//        imageView2.animate()
//            .scaleX(w.toFloat())
//            .setDuration(0)
//            .start()
//    }

    private fun setRandomSize(imageView: ImageView){
        var random = ((width - (50 * 8))..(width + (50 * 8))).random()
        var par = imageView1.layoutParams
        par.width = random
        imageView1.layoutParams = par
    }

    private fun setImageWidth(w: Int) {
        var par = imageView2.layoutParams
        par.width = w
        imageView2.layoutParams = par


    }

    private fun newGame() {
        progress2.setImageResource(R.color.colorOK)
        gameField.animate()
            .alpha(0.1F)
            .setDuration(0)
            .start()

        gameField.visibility = View.VISIBLE

        gameField.animate()
            .alpha(1F)
            .setDuration(1000)
            .start()
    }

    private fun randomImage() {
        percent.text = "0%"
        done.setImageResource(R.drawable.done1)
        progress2.animate()
            .translationX(progress1.x - progress1.width)
            .setDuration(0)
            .start()
        size.progress = 50
        imageView1.visibility = View.INVISIBLE
        imageView2.visibility = View.INVISIBLE
//        imageView2.layoutParams = imageView1.layoutParams
        if (_width) {
            width = imageView1.width
            _width = false
        }

        shadow.visibility = View.INVISIBLE
        win.visibility = View.INVISIBLE
        lose.visibility = View.INVISIBLE
        check.visibility = View.INVISIBLE
        size.visibility = View.INVISIBLE
        good2.visibility = View.INVISIBLE

        var random = getRandom()
        rand--
        image1 = list1.get(random)
        list1.remove(image1)

        image2 = list2.get(random)
        list2.remove(image2)
        Log.e("${list1.size}", "${list1.size}")
        imageView1.setImageResource(image1)
         setRandomSize(imageView1)
//        imageView3.setImageResource(image1)
        imageView2.setImageResource(image2)

        start()
    }

    private fun start() {
        time.text = "3"

        showNew()

        imageView1.animate()
            .rotation(randomRotate().toFloat())
            .setDuration(0)
            .start()

        object : CountDownTimer(4000, 100) {
            override fun onFinish() {

                showGame()
                newGame()
                imageView2.visibility = View.VISIBLE
            }

            override fun onTick(p0: Long) {
                time.text = "${p0 / 1000}"
                if(p0 < 1400){

                    this.onFinish()
                    this.cancel()
                }
            }

        }.start()
    }

    private fun randomRotate() = (0..359).random()

    private fun getRandom() = (0..rand).random()

    private fun showNew() {
        score.visibility = View.INVISIBLE
        check.visibility = View.INVISIBLE
        size.visibility = View.INVISIBLE
        imageView2.visibility = View.INVISIBLE

        time.visibility = View.VISIBLE
        imageView1.visibility = View.VISIBLE

        gameField.isClickable = false
    }

    private fun showGame() {
        imageView2.visibility = View.VISIBLE
        score.visibility = View.VISIBLE
        check.visibility = View.VISIBLE
        size.visibility = View.VISIBLE

        time.visibility = View.INVISIBLE
        imageView1.visibility = View.INVISIBLE

        gameField.isClickable = true
        check.isClickable = true
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onClick(p0: View?) {
        gameField.isClickable = false
        check.isClickable = false

        checkImage()

//        checkImage3()

        Handler().postDelayed({
            calculate()
        }, 1500)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun calculate() {
        Log.e("${imageView1.width}", "${imageView2.width}")

        var c = (imageView2.width * 100) / imageView1.width
        if (c > 100) {
            c = 200 - (imageView2.width * 100) / imageView1.width
        }

        percent.text = "$c%"
        if (c < 80){
            progress2.setImageResource(R.color.colorFALSE)
        }

        var anim = progress2.animate()

        anim.setUpdateListener {
            var text =  (((progress2.x + progress1.width)  * 100) / progress1.width).toInt()
            if (text < 0){
                text = 0
            }
            percent.text = "$text%"
        }

                anim
            .translationX( ((progress1.width * c)/100 - progress1.width).toFloat() )
            .setDuration(1500)
            .start()

                if (c >= 80) {
                    Handler().postDelayed({
                        done.setImageResource(R.drawable.done2)
                        done.animate()
                            .scaleX(2F)
                            .scaleY(2F)
                            .setDuration(300)
                            .withEndAction{
                                done.animate()
                                    .scaleX(1F)
                                    .scaleY(1F)
                                    .setDuration(300)
                                    .start()
                            }
                            .start()
                    }, 1000)

                }
                if (c >= 95) {
                    Handler().postDelayed({
                        good2.visibility = View.VISIBLE
                        good2.animate()
                            .scaleX(2F)
                            .scaleY(2F)
                            .setDuration(300)
                            .withEndAction{
                                good2.animate()
                                    .scaleX(1F)
                                    .scaleY(1F)
                                    .setDuration(300)
                                    .start()
                            }
                            .start()
                    },1250)

                }




        Handler().postDelayed({
            end(c)
        }, 1800)
    }

    private fun end(calculate: Int) {
        Handler().postDelayed({
            shadow.visibility = View.VISIBLE
            check.visibility = View.INVISIBLE
            size.visibility = View.INVISIBLE
            Log.e("ZZZZZZZ", "ZZZZZZZZ")
            if (calculate >= 80) {
                Log.e("XXXXXXX", "XXXXXXXXX")
                lvl++
                if (lvl == 26) {
                    gameOver()
                } else {
                    showWin()
                }
            } else {
                Log.e("CCCCCCC", "VVVVV")
                showLose()
            }
        }, 600)

    }

    private fun showWin() {
        win.visibility = View.VISIBLE
    }

    private fun showLose() {
        lose.visibility = View.VISIBLE
    }

    private fun gameOver() {
        Handler().postDelayed({
            check.visibility = View.INVISIBLE
            size.visibility = View.INVISIBLE
            shadow.visibility = View.VISIBLE
            win.visibility = View.VISIBLE
            lose.visibility = View.VISIBLE
            you_lose.visibility = View.INVISIBLE
            you_win.visibility = View.VISIBLE
            next.visibility = View.INVISIBLE
        }, 850)
    }

    private fun checkImage() {
        if (imageView1.width > imageView2.width){
            imageView1.setImageResource(image1)
            imageView2.setImageResource(image2)
        }else {
            imageView1.setImageResource(image2)
            imageView2.setImageResource(image1)
        }
        imageView1.animate()
            .alpha(0F)
            .setDuration(0)
            .start()
        imageView1.visibility = View.VISIBLE
        imageView1.animate()
            .alpha(1F)
            .setDuration(250)
            .start()

        Handler().postDelayed({
            imageView1.animate()
                .rotation(360F)
                .setDuration(1000)
                .start()
        }, 250)

        Handler().postDelayed({
            imageView1.setImageResource(image2)
            imageView2.setImageResource(image1)
        }, 1400)

        Handler().postDelayed({
            imageView1.setImageResource(image1)
            imageView2.setImageResource(image2)
        }, 1800)

        Handler().postDelayed({
            imageView1.setImageResource(image2)
            imageView2.setImageResource(image1)
        }, 2200)

        Handler().postDelayed({
            imageView1.setImageResource(image1)
            imageView2.setImageResource(image2)
        }, 2600)

        Handler().postDelayed({
            if (imageView1.width > imageView2.width){
                imageView1.setImageResource(image1)
                imageView2.setImageResource(image2)
            }else {
                imageView1.setImageResource(image2)
                imageView2.setImageResource(image1)
            }
        }, 3000)

    }

//    private fun checkImage3(){
//            imageView3.animate()
//                .scaleX(0.1F)
//                .scaleY(0.1F)
//                .setDuration(0)
//                .start()
//
//        imageView3.visibility = View.VISIBLE
//
//        Handler().postDelayed({
//            imageView1.animate()
//                .scaleX(1F)
//                .scaleY(1F)
//                .setDuration(500)
//                .start()
//        },1500)
//    }
}
